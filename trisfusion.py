def fusion_sorted(L1,L2):
    return sorted(L1+L2)

def fusion2(tab1,tab2):
    tab=[]
    n1=len(tab1)
    n2=len(tab2)
    i1,i2=0,0
    n=n1+n2
    for i in range(n):
        if i1<n1 and i2<n2:
            if tab1[i1]<=tab2[i2]:
                tab.append(tab1[i1])
                i1+=1
            else:
                tab.append(tab2[i2])
                i2+=1
        elif i1>=n1:
            tab.append(tab2[i2])
            i2+=1
        else:
            tab.append(tab1[i1])
            i1+=1
    return tab

def fusion3(tab1,tab2):
    tab=[]
    n1=len(tab1)
    n2=len(tab2)
    i1,i2=0,0
    n=n1+n2
    for i in range(n):
        if i1<n1 and i2<n2:
            if tab1[i1]<=tab2[i2]:
                tab.append(tab1[i1])
                i1+=1
            else:
                tab.append(tab2[i2])
                i2+=1
        elif i1>=n1:
            tab += tab2[i2:]
            break
        else:
            tab += tab1[i1:]
            break
    return tab

def tri_fusion_sorted(tableau):
    if  len(tableau) <= 1: 
        return tableau
    pivot = len(tableau)//2
    tableau1 = tableau[:pivot]
    tableau2 = tableau[pivot:]
    gauche = tri_fusion_sorted(tableau1)
    droite = tri_fusion_sorted(tableau2)
    fusionne = fusion_sorted(gauche,droite)
    return fusionne

def tri_fusion2(tableau):
    if  len(tableau) <= 1: 
        return tableau
    pivot = len(tableau)//2
    tableau1 = tableau[:pivot]
    tableau2 = tableau[pivot:]
    gauche = tri_fusion2(tableau1)
    droite = tri_fusion2(tableau2)
    fusionne = fusion2(gauche,droite)
    return fusionne

def tri_fusion3(tableau):
    if  len(tableau) <= 1: 
        return tableau
    pivot = len(tableau)//2
    tableau1 = tableau[:pivot]
    tableau2 = tableau[pivot:]
    gauche = tri_fusion3(tableau1)
    droite = tri_fusion3(tableau2)
    fusionne = fusion3(gauche,droite)
    return fusionne
