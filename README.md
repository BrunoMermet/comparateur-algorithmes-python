# Petit utilitaire permettant de comparer, graphiquement, l'efficacité en temps de plusieurs algorithmes

# Contenu
Le programme tient dans le fichier `comparaisonalgo.py`. # les fichiers `trisfusion.py` et `comparaison_tris.py` sont là pour illustrer l'utilisation du programme.

# Utilisation
## Création des algorithmes
Il faut commencer par créer des instances de la classe `Algorithme` pour chacun des algorithmes à comparer. Les 2 paramètres à passer sont :
* le nom que l'on souhaite donner à l'algorithme ;
* la fonction implantant l'algorithme

## Définition du générateur
Il faut ensuite définir un *générateur de paramètres*. C'est une fonction qui doit prendre en paramètre un naturel (qui correspondra à l'abscisse lors du tracé sur le graphique) et qui renvoie la liste des paramètres à passer aux fonctions implantant l'algorithme pour la valeur qui lui est passée.

## Définitions des valeurs de test
Il faut maintenant définir la liste des valeurs pour lesquels on souhaite tester les algorithmes.

## Nombre de tests par valeur
Un dernier paramètre à définir est le nombre de tests que l'on souhaite effectuer pour chaque valeur.

## Exécution
L'exécution et la génération du graphique ce fait alors en passant en paramètres à la méthode `Comparateur.analyser` les paramètres précédemment définis :
* la liste des algorithmes à comparer ;
* la fonction générateur de données ;
* la liste des valeurs pour lesquelles faire le test ;
* le nombre de tests à effectuer par valeur

Le graphique est affiché à la fin de l'exécution grâce à *matplotlib*.

# Licence
Licence CC BY-NC-SA