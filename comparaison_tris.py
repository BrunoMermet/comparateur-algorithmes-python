from random import randint
from comparaisonalgo import Algorithme, Comparateur
from trisfusion import tri_fusion_sorted, tri_fusion2, tri_fusion3

NB_TESTS = 4
DEBUT = 0
FIN   = 20001
PAS   = 5000
VAL_MAX = 10000

def generateur_donnees(taille):
    # Le générateur des données.
    # Doit renvoyer la liste des paramètres à passer à l'algo.
    # Comme ici, l'algo attend une liste, il faut renvoyer une liste
    # de listes.
    resultat = [[randint(0, VAL_MAX) for _ in range(taille)]]
    return resultat

algo1 = Algorithme(tri_fusion_sorted, "tri fusion sorted")
algo2 = Algorithme(tri_fusion2, "tri fusion 2")
algo3 = Algorithme(tri_fusion3, "tri fusion 3")

abscisses = [n for n in range(DEBUT, FIN, PAS)]

if __name__ == "__main__":
    Comparateur.analyser([algo1, algo2, algo3], generateur_donnees, abscisses, NB_TESTS)
