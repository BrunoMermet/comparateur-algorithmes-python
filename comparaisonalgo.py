from time import time
import matplotlib.pyplot as plt

class Algorithme:
    '''
    Classe permettant de définir un algorithme et d'évaluer
    let temps de calcul qu'il prend sur un jeu de données.
    Un algorithme est caractérisé par la fonction qui l'implante
    et un nom.
    '''
    def __init__(self, fonction, nom):
        self.fonction = fonction
        self.nom = nom

    def temps_unitaire(self, donnee):
        debut = time()
        self.fonction(*donnee)
        fin = time()
        return fin - debut

    def temps_moyen(self, donnees, nb_essais = 1):
        assert nb_essais > 0
        temps_total = 0
        for _ in nb_essais:
            temps_total += temps_unitaire(donnees)
        return temps_total / nb_essais


class Comparateur:
    '''
    Classe permettant de comparer l'efficacité de différents algorithmes.
    Un comparateur est défini par une liste d'algorithme, une fonction
    de génération de données par rapport à un paramètre, la liste des
    paramètres à partir desquels générer les données, et le nombre
    d'essais que l'on compte faire pour chaque paramètre.
    Un générateur est une fonction qui doit, pour un paramètre donnée,
    générer la liste des paramètres à passer à l'algorithme.
    '''
    def __init__(self, liste_algorithmes, generateur, abscisses, nb_essais = 1):
        self.liste_algorithmes = liste_algorithmes
        self.generateur = generateur
        self.abscisses = abscisses
        self.nb_essais = nb_essais
        self.resultats = None

    def comparer(self):
        self.resultats = []
        for _ in range(len(self.liste_algorithmes)):
            self.resultats.append([])
        for parametre_generateur in self.abscisses:
            cumul_temps = [0] * len(self.liste_algorithmes)
            for _ in range(self.nb_essais):
                donnee = self.generateur(parametre_generateur)
                for num_algo in range(len(self.liste_algorithmes)):
                    algo = self.liste_algorithmes[num_algo]
                    cumul_temps[num_algo] += algo.temps_unitaire(donnee)
            for num_algo in range(len(self.liste_algorithmes)):
                temps_moyen = cumul_temps[num_algo] / self.nb_essais
                self.resultats[num_algo].append(temps_moyen)

    def tracer(self):
        assert self.resultats is not None
        ordonnee_max = max(list(map(lambda liste: max(liste), self.resultats)))
        for num_algo in range(len(self.liste_algorithmes)):
            nom_algo = self.liste_algorithmes[num_algo].nom
            ordonnees = self.resultats[num_algo]
            plt.plot(self.abscisses, ordonnees, label=nom_algo)
        plt.xlim(0, self.abscisses[-1])
        plt.ylim(0, ordonnee_max)
        plt.legend()
        plt.show()
        
    def analyser(liste_algorithmes, generateur, abscisses, nb_essais = 1):
        comparateur = Comparateur(liste_algorithmes, generateur, abscisses, nb_essais)
        comparateur.comparer()
        comparateur.tracer()
